Addresses
=========

It's a laboratory work on programming for NArFU. This project demonstrates how an application can interact with
different types of the data storage. The program is adapted to work with two data sources:

- Microsoft Excel (XLSX files)
- MySQL

Sample files with appropriate structure are present for both.


Additional features
-------------------

As a bonus, the application is able to start a web server and display data not only in its windows but in the browser
as well.


Used technologies
-----------------

The project is fully implemented in the [Kotlin] programming language and intended to be run on the JVM platform.
To work with XLSX files, the [Apache POI] library was chosen.

The user interface is built as a JavaFX application using specialized UI library named [TornadoFX].

[Kotlin]: https://kotlinlang.org
[TornadoFX]: https://tornadofx.io
[Apache POI]: https://poi.apache.org


Screenshot
----------

![Main window](http://kozalo.ru/images/posts/205416181.png)
