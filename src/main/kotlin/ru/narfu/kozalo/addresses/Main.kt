package ru.narfu.kozalo.addresses

import tornadofx.App
import ru.narfu.kozalo.addresses.views.MainView


class Application : App(MainView::class)
