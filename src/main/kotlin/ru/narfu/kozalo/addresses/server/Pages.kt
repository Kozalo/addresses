package ru.narfu.kozalo.addresses.server

import kotlinx.html.*
import kotlinx.html.stream.appendHTML
import ru.narfu.kozalo.addresses.data.GeographicalObject
import ru.narfu.kozalo.addresses.data.GeographicalObjectType


fun indexPage() = buildString {
    doctype()
    appendHTML().html {
        head {
            title("Главная страница")
        }
        body {
            ul {
                li {
                    a("/geographical-objects") {
                        text("Географически объекты")
                    }
                }
                li {
                    a("/types") {
                        text("Типы географических объектов")
                    }
                }
                li {
                    a("/tree") {
                        text("Дерево географических объектов")
                    }
                }
            }
        }
    }
}

fun geographicalObjectsPage(geographicalObjects: List<GeographicalObject>) = buildString {
    doctype()
    appendHTML().html {
        head {
            title("Географические объекты")
            tableStyles()
        }
        body {
            table {
                thead {
                    tr {
                        th { text("Идентификатор") }
                        th { text("Тип") }
                        th { text("Наименование") }
                        th { text("Родитель") }
                        th { text("Полный адрес") }
                    }
                }
                tbody {
                    geographicalObjects.forEach { geographicalObject ->
                        tr {
                            td { text(geographicalObject.id) }
                            td { text(geographicalObject.type.fullName) }
                            td { text(geographicalObject.name) }
                            td { text(geographicalObject.parent?.name ?: "") }
                            td { text(geographicalObject.fullAddress) }
                        }
                    }
                }
            }
        }
    }
}

fun typesPage(geographicalObjectTypes: List<GeographicalObjectType>) = buildString {
    doctype()
    appendHTML().html {
        head {
            title("Типы географических объектов")
            tableStyles()
        }
        body {
            table {
                thead {
                    tr {
                        th { text("Идентификатор") }
                        th { text("Наименование") }
                        th { text("Сокращение") }
                    }
                }
                tbody {
                    geographicalObjectTypes.forEach { geographicalObjectType ->
                        tr {
                            td { text(geographicalObjectType.id) }
                            td { text(geographicalObjectType.fullName) }
                            td { text(geographicalObjectType.shortName) }
                        }
                    }
                }
            }
        }
    }
}

fun treePage(geographicalObjects: List<GeographicalObject>) = buildString {
    doctype()
    appendHTML().html {
        head {
            title("Дерево географических объектов")
        }
        body {
            ul {
                val roots = geographicalObjects.filter { it.parent == null }
                roots.forEach { root ->
                    populate(geographicalObjects, root)
                }
            }
        }
    }
}


private fun StringBuilder.doctype() = appendln("<!DOCTYPE html>")

private fun HEAD.tableStyles() = style { +"""
    |table {
    |   border-collapse: collapse;
    |}
    |table, th, td {
    |   border: 1px solid black;
    |   padding: 2px 3px;
    |}
    |""".trimMargin()
}

private fun UL.populate(objects: List<GeographicalObject>, root: GeographicalObject) {
    li { text(root.fullName) }
    ul {
        objects.filter { it.parent == root }.forEach {
            populate(objects, it)
        }
    }
}
