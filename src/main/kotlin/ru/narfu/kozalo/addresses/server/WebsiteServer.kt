package ru.narfu.kozalo.addresses.server

import com.sun.net.httpserver.HttpExchange
import com.sun.net.httpserver.HttpServer
import java.io.IOException
import java.net.InetSocketAddress
import java.nio.charset.StandardCharsets
import kotlin.collections.mutableMapOf
import kotlin.collections.set
import kotlin.collections.toMap


interface WebsiteServer {
    val port: Int
    fun stop()
}


class SimpleWebsiteServer(processors: Processors) : WebsiteServer {

    private val server = HttpServer.create(InetSocketAddress(0), 0)

    override val port = server.address.port

    init {
        processors.asMap().forEach { path, processor ->
            server.createContext(path, buildRequestProcessor(processor))
        }
        server.start()
    }

    override fun stop() = server.stop(0)

    private fun buildRequestProcessor(processor: (HttpExchange) -> String): (HttpExchange) -> Unit = { httpExchange ->
        val response = processor(httpExchange)
        val bytes = response.toByteArray(StandardCharsets.UTF_8)
        httpExchange.responseHeaders["Content-Type"] = "text/html; charset=utf-8"
        try {
            httpExchange.sendResponseHeaders(200, bytes.size.toLong())
            httpExchange.responseBody.use { it.write(bytes) }
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

}


class Processors {

    private val processors = mutableMapOf<String, (HttpExchange) -> String>()

    fun asMap() = processors.toMap()

    fun route(path: String, action: (HttpExchange) -> String) {
        processors[path] = action
    }

}

fun requestProcessors(body: Processors.() -> Unit) = Processors().apply(body)
