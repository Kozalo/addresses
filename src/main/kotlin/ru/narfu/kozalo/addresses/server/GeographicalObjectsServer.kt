package ru.narfu.kozalo.addresses.server

import ru.narfu.kozalo.addresses.data.GeographicalObject
import ru.narfu.kozalo.addresses.data.GeographicalObjectType


class GeographicalObjectsServer(private val geographicalObjects: List<GeographicalObject>,
                                private val geographicalObjectTypes: List<GeographicalObjectType>) : WebsiteServer {

    private val processors = requestProcessors {
        route("/") {
            indexPage()
        }
        route("/geographical-objects") {
            geographicalObjectsPage(geographicalObjects)
        }
        route("/types") {
            typesPage(geographicalObjectTypes)
        }
        route("/tree") {
            treePage(geographicalObjects)
        }
    }

    private val server = SimpleWebsiteServer(processors)

    override val port = server.port

    override fun stop() = server.stop()

}
