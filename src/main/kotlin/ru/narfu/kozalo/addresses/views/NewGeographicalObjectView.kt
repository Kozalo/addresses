package ru.narfu.kozalo.addresses.views

import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import ru.narfu.kozalo.*
import ru.narfu.kozalo.addresses.GeographicalObjectCreated
import ru.narfu.kozalo.addresses.data.GeographicalObject
import ru.narfu.kozalo.addresses.data.GeographicalObjectType
import tornadofx.*


class NewGeographicalObjectView(private val objectId: Int,
                                private val types: List<GeographicalObjectType>,
                                private val parents: List<GeographicalObject>)
    : View("Создание географического объекта") {

    private val objectType = SimpleObjectProperty<GeographicalObjectType?>()
    private val objectName = SimpleStringProperty()
    private val objectParent = SimpleObjectProperty<GeographicalObject?>()

    override val root = form {
        prefWidth = 450.0

        fieldset("Географический объект №$objectId") {
            field("Тип") {
                combobox(objectType, types) {
                    cellFormat { text = it?.fullName }
                }
            }
            field("Наименование") {
                textfield().bind(objectName)
            }
            field("Родительский объект") {
                combobox(objectParent, parents) {
                    cellFormat { text = it?.fullName }
                }
            }
            buttonbar {
                paddingTop = 10.0
                button("Создать") {
                    enableWhen(objectName.isNotEmpty.and(objectType.isNotNull))
                    action {
                        val newObject = GeographicalObject(objectId, objectType.value!!, objectName.value!!)
                        newObject.parent = objectParent.value
                        fire(GeographicalObjectCreated(newObject))
                        close()
                    }
                }
                button("Отменить").action { close() }
            }
        }
    }
}
