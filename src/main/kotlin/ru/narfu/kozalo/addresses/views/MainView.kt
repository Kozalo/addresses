package ru.narfu.kozalo.addresses.views

import javafx.scene.control.TabPane
import ru.narfu.kozalo.addresses.AppController
import ru.narfu.kozalo.addresses.InvalidateGeographicalObjectsTable
import ru.narfu.kozalo.addresses.OpenWebsite
import ru.narfu.kozalo.addresses.PauseView
import ru.narfu.kozalo.addresses.data.GeographicalObject
import ru.narfu.kozalo.addresses.data.GeographicalObjectType
import ru.narfu.kozalo.addresses.data.sources.SHEET_GO
import ru.narfu.kozalo.addresses.data.sources.SHEET_GO_TYPES
import tornadofx.*


class MainView : View("Географические объекты") {

    private val controller: AppController by inject()

    private val geographicalObjectModel: GeographicalObjectModel by inject()

    override val root = borderpane {
        prefWidth = 600.0
        prefHeight = 400.0

        top = menubar {
            menu("Файл") {
                menu("Открыть") {
                    item("XLSX-файл").action(controller::openExcelFile)
                    item("Базу данных...").action(controller::openMySqlDatabase)
                }
                item("Сохранить") {
                    enableWhen(controller.dataSourceLoaded)
                    action(controller::save)
                }
            }
            menu("Сервер") {
                val serverState = checkmenuitem("Сервер включён") {
                    selectedProperty().onChange(controller::toggleServer)
                }
                item("Открыть сайт в браузере") {
                    enableWhen(serverState.selectedProperty())
                    action { fire(OpenWebsite) }
                }
                separator()
                item(controller.port) {
                    isDisable = true
                }
            }
        }

        center = tabpane {
            tabClosingPolicy = TabPane.TabClosingPolicy.UNAVAILABLE

            tab(SHEET_GO) {
                tableview(controller.geographicalObjects) {
                    columnResizePolicy = SmartResize.POLICY

                    column("№", GeographicalObject::id).contentWidth(useAsMin = true, useAsMax = true)
                    column("№ типа ГО", GeographicalObject::typeId)
                    column("Наименование", GeographicalObject::name)
                    column("Полный адрес", GeographicalObject::fullAddress)

                    bindSelected(geographicalObjectModel)
                    bindSelected(controller.selectedGeographicalObject)

                    onDoubleClick(controller::openEditGeographicalObjectView)

                    contextmenu {
                        item("Добавить").action(controller::addGeographicalObject)
                        item("Удалить").action(controller::deleteGeographicalObject)
                    }

                    subscribe<InvalidateGeographicalObjectsTable> {
                        refresh()
                    }
                }
            }

            tab(SHEET_GO_TYPES) {
                tableview(controller.geographicalObjectTypes) {
                    columnResizePolicy = SmartResize.POLICY
                    enableCellEditing()

                    column("№", GeographicalObjectType::id).contentWidth(useAsMin = true, useAsMax = true)
                    column("Наименование", GeographicalObjectType::fullName).remainingWidth().makeEditable()
                    column("Сокращение", GeographicalObjectType::shortName).makeEditable()

                    bindSelected(controller.selectedGeographicalObjectType)

                    contextmenu {
                        item("Добавить").action(controller::addGeographicalObjectType)
                        item("Удалить").action(controller::deleteGeographicalObjectType)
                    }

                    onEditCommit { controller.editGeographicalObjectType(it) }
                }
            }

            tab("Дерево ГО") {
                controller.treeView = treeview {
                    cellFormat { text = it.fullName }
                }
            }
        }
    }

    init {
        subscribe<PauseView> {
            replaceWith<ProgressView>()
        }
    }

}
