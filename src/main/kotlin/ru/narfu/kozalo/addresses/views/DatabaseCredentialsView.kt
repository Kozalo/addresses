package ru.narfu.kozalo.addresses.views

import javafx.application.Platform
import javafx.beans.property.*
import javafx.scene.paint.Color
import ru.narfu.kozalo.addresses.LoginToDatabase
import ru.narfu.kozalo.addresses.DataSourceLoaded
import tornadofx.*


class DatabaseCredentialsView : View("Данные для подключения к базе данных") {

    private val databaseName = SimpleStringProperty()
    private val login = SimpleStringProperty()
    private val password = SimpleStringProperty()

    var onCloseCallback: (() -> Unit)? = null

    init {
        subscribe<DataSourceLoaded> {
            closeForm()
        }
    }

    override val root = form {
        fieldset("Введите необходимую для входа информацию") {
            field("Название базы") {
                textfield().bind(databaseName)
            }
            field("Логин") {
                textfield().bind(login)
            }
            field("Пароль") {
                passwordfield().bind(password)
            }

            buttonbar {
                button("Войти") {
                    enableWhen(databaseName.isNotEmpty.and(login.isNotEmpty))
                    shortcut("Enter")
                    action {
                        fire(LoginToDatabase(databaseName.value, login.value, password.value))
                    }
                }
                button("Отмена").action { closeForm() }
            }
        }

        style {
            borderColor += box(Color.BLACK)
        }
    }

    private fun closeForm() {
        onCloseCallback?.invoke()
        // We have to close the form a bit later in outer context to avoid a fatal error.
        Platform.runLater {
            close()
        }
    }

}
