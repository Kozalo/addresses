package ru.narfu.kozalo.addresses.views

import ru.narfu.kozalo.addresses.data.GeographicalObject
import ru.narfu.kozalo.addresses.GeographicalObjectChanged
import ru.narfu.kozalo.addresses.data.GeographicalObjectType
import ru.narfu.kozalo.addresses.InvalidateGeographicalObjectsTable
import tornadofx.*


class EditGeographicalObjectView(private val types: List<GeographicalObjectType>,
                                 private val parents: List<GeographicalObject>)
    : View("Редактирование географического объекта") {

    private val model: GeographicalObjectModel by inject()

    override val root = form {
        prefWidth = 450.0

        fieldset("Географический объект №${model.id.value}") {
            field("Тип") {
                combobox(model.type, types) {
                    cellFormat { text = it.fullName }
                }
            }
            field("Наименование") {
                textfield().bind(model.name)
            }
            field("Родительский объект") {
                val comboBoxParents = combobox(model.parent, parents) {
                    cellFormat { text = it.fullName }
                }
                button("X").action {
                    comboBoxParents.value = null
                }
            }
            buttonbar {
                paddingTop = 10.0
                button("Применить") {
                    enableWhen(model.dirty)
                    action {
                        model.commit()
                        fire(InvalidateGeographicalObjectsTable)
                        fire(GeographicalObjectChanged(model.item))
                        close()
                    }
                }
                button("Отменить").action {
                    model.rollback()
                    close()
                }
            }
        }
    }

}


class GeographicalObjectModel : ItemViewModel<GeographicalObject>() {
    val id = bind(GeographicalObject::id)
    val type = bind(GeographicalObject::type)
    val name = bind(GeographicalObject::name)
    val parent = bind(GeographicalObject::parent)

    override fun onCommit() {
         item.parent = parent.value
    }
}
