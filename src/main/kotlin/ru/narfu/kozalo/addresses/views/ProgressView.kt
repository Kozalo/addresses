package ru.narfu.kozalo.addresses.views

import javafx.geometry.Pos
import ru.narfu.kozalo.addresses.ResumeView
import tornadofx.*


class ProgressView : View("Выполняется операция") {

    override val root = vbox {
        progressindicator().apply {
            maxWidth = 50.0
            maxHeight = 50.0
        }
        label("Пожалуйста, подождите...")

        alignment = Pos.CENTER
        spacing = 10.0
    }

    init {
        subscribe<ResumeView> {
            replaceWith<MainView>()
        }
    }

}
