package ru.narfu.kozalo.addresses.views

import javafx.beans.property.SimpleStringProperty
import ru.narfu.kozalo.addresses.data.GeographicalObjectType
import ru.narfu.kozalo.addresses.GeographicalObjectTypeCreated
import tornadofx.*


class NewGeographicalObjectTypeView(private val typeId: Int)
    : View("Создание нового типа географических объектов") {

    private val typeFullName = SimpleStringProperty()
    private val typeShortName = SimpleStringProperty()

    override val root = form {
        prefWidth = 450.0

        fieldset("Тип №$typeId") {
            field("Наименование") {
                textfield().bind(typeFullName)
            }
            field("Сокращение") {
                textfield().bind(typeShortName)
            }
            buttonbar {
                paddingTop = 10.0
                button("Создать") {
                    enableWhen(typeFullName::isNotEmpty)
                    action {
                        val newType = GeographicalObjectType(typeId, typeFullName.value!!, typeShortName.value ?: "")
                        fire(GeographicalObjectTypeCreated(newType))
                        close()
                    }
                }
                button("Отменить").action { close() }
            }
        }
    }

}
