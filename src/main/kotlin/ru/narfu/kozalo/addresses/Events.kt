package ru.narfu.kozalo.addresses

import ru.narfu.kozalo.addresses.data.GeographicalObject
import ru.narfu.kozalo.addresses.data.GeographicalObjectType
import tornadofx.*
import tornadofx.EventBus.RunOn.BackgroundThread


object InvalidateGeographicalObjectsTable : FXEvent()

class GeographicalObjectCreated(val obj: GeographicalObject) : FXEvent()
class GeographicalObjectChanged(val obj: GeographicalObject) : FXEvent(BackgroundThread)

class GeographicalObjectTypeCreated(val type: GeographicalObjectType) : FXEvent()
class GeographicalObjectTypeChanged(val type: GeographicalObjectType) : FXEvent(BackgroundThread)

class LoginToDatabase(val databaseName: String, val login: String, val password: String?) : FXEvent()
object DataSourceLoaded : FXEvent()

object OpenWebsite : FXEvent()

object PauseView : FXEvent()
object ResumeView : FXEvent()
