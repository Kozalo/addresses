package ru.narfu.kozalo.addresses

import javafx.application.Platform
import javafx.scene.control.Alert
import javafx.scene.control.Alert.AlertType
import javafx.scene.control.Alert.AlertType.*
import javafx.stage.Modality


fun showWarning(text: String) = showMessage(WARNING, text)

fun showError(text: String, e: Throwable) = showMessage(ERROR, "$text\n\n${e.message}")


private fun showMessage(type: AlertType, text: String) {
    Platform.runLater {
        Alert(type, text).apply {
            initModality(Modality.WINDOW_MODAL)
            isResizable = true
        }.show()
    }
}
