package ru.narfu.kozalo.addresses.data.manipulators

import ru.narfu.kozalo.addresses.data.GeographicalObject


class DataValidator(private val geographicalObjects: List<GeographicalObject>) {

    var issue: String = ""
        private set

    // TODO: Add more checks.
    fun isDataValid(): Boolean {
        val roots = geographicalObjects.count { it.parent == null }
        return when {
            roots > 1 -> {
                issue = "Два корневых объекта не допустимы!"
                false
            }
            roots < 1 -> {
                issue = "Нет корневого Объекта!"
                false
            }
            else -> true
        }
    }

}
