package ru.narfu.kozalo.addresses.data.sources

import javafx.stage.FileChooser
import org.apache.poi.EmptyFileException
import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.xssf.usermodel.XSSFSheet
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import ru.narfu.kozalo.addresses.data.GeographicalObject
import ru.narfu.kozalo.addresses.data.GeographicalObjectType
import ru.narfu.kozalo.addresses.showError
import ru.narfu.kozalo.addresses.showWarning
import tornadofx.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.nio.file.*


internal const val SHEET_GO = "ГО"
internal const val SHEET_GO_TYPES = "Типы ГО"


class ExcelLoader : DataSourceLoader<GeographicalObject, GeographicalObjectType> {

    private var file: File? = null

    override fun prepareLoading() {
        file = with(FileChooser()) {
            title = "Выберите файл с таблицей..."
            initialDirectory = rootDirPath.toFile()
            extensionFilters += FileChooser.ExtensionFilter("Рабочая книга Excel (*.xlsx)", listOf("*.xlsx"))
            showOpenDialog(FX.primaryStage)
        }
    }

    override fun load() = file?.let { ExcelDataSource(it, this) }

    private val rootDirPath: Path = run {
        var path = Paths.get(javaClass.protectionDomain.codeSource.location.toURI())
        while (!Files.isDirectory(path)) {
            path = path.parent
        }
        path
    }

}


class ExcelSaver<T, K>(private val file: File, private val workbook: XSSFWorkbook,
                       private val dataSource: DataSource<T, K>) : DataSourceSaver {

    private val onWindows = System.getProperty("os.name").startsWith("Windows")

    @Throws(IOException::class)
    override fun save() {
        val tmpFile = File.createTempFile(file.nameWithoutExtension, file.extension)
        FileOutputStream(tmpFile).use { outputStream ->
            workbook.write(outputStream)
        }
        if (onWindows) {
            dataSource.close()
        }
        Files.move(tmpFile.toPath(), file.toPath(), StandardCopyOption.REPLACE_EXISTING)
    }

}


class ExcelDataSource(file: File, override val loader: DataSourceLoader<GeographicalObject, GeographicalObjectType>)
    : DataSource<GeographicalObject, GeographicalObjectType> {

    private val workbook = try {
        XSSFWorkbook(file)
    } catch (e: EmptyFileException) {
        showWarning("Файл пуст!")
        XSSFWorkbook().createTitledSheet(SHEET_GO, "№", "№ типа ГО", "Наименование", "Родительский ГО")
                .createTitledSheet(SHEET_GO_TYPES, "№", "Наименование", "Сокращение")
    }

    private var geographicalObjectTypesCache: List<GeographicalObjectType>? = null


    override val saver: DataSourceSaver = ExcelSaver(file, workbook, this)

    override var isClosed = false
        private set


    override fun getGeographicalObjects(useCache: Boolean): List<GeographicalObject> {
        val geographicalObjects = mutableListOf<GeographicalObject>()
        val geographicalObjectTypes = getGeographicalObjectTypes(useCache)
        val childToParent = mutableMapOf<Int, Int>()

        try {
            forEachRowInSheet(SHEET_GO) {
                val id = getCell(0).numericCellValue.toInt()
                val typeId = getCell(1).numericCellValue.toInt()
                val type = geographicalObjectTypes.find { it.id == typeId }!!
                val name = try {
                    getCell(2).stringCellValue
                } catch (e: IllegalStateException) {
                    getCell(2).numericCellValue.toInt().toString()
                }
                getCell(3)?.numericCellValue?.toInt()?.let {
                    childToParent[id] = it
                }
                geographicalObjects += GeographicalObject(id, type, name)
            }
        } catch (e: Exception) {
            e.printStackTrace()
            showError("Нарушение формата файла!", e)
        }

        geographicalObjects.filter { it.id in childToParent }
                .forEach { obj ->
                    obj.parent = geographicalObjects.find { it.id == childToParent[obj.id] }
                }

        return geographicalObjects
    }

    override fun getGeographicalObjectTypes(useCache: Boolean): List<GeographicalObjectType> {
        val cache = geographicalObjectTypesCache
        return if (useCache && cache != null) {
            cache
        } else {
            val types = mutableListOf<GeographicalObjectType>()
            try {
                forEachRowInSheet(SHEET_GO_TYPES) {
                    val id = getCell(0).numericCellValue.toInt()
                    val name = getCell(1).stringCellValue
                    val shortName = getCell(2)?.stringCellValue ?: ""
                    types += GeographicalObjectType(id, name, shortName)

                }
            } catch (e: IllegalStateException) {
                e.printStackTrace()
                showError("Нарушение формата файла!", e)
            }
            types.apply { geographicalObjectTypesCache = this }
        }
    }

    override fun addGeographicalObject(obj: GeographicalObject) {
        forSheet(SHEET_GO) {
            with (createRow(lastRowNum + 1)) {
                createCell(0).apply {
                    setCellType(CellType.NUMERIC)
                    setCellValue(obj.id.toDouble())
                }
                createCell(1).apply {
                    setCellType(CellType.NUMERIC)
                    setCellValue(obj.typeId.toDouble())
                }
                createCell(2).apply {
                    setCellType(CellType.STRING)
                    setCellValue(obj.name)
                }
                createCell(3).apply {
                    setCellType(CellType.NUMERIC)
                    obj.parent?.id?.let {
                        setCellValue(it.toDouble())
                    }
                }
            }
        }
    }

    override fun addGeographicalObjectType(type: GeographicalObjectType) {
        forSheet(SHEET_GO_TYPES) {
            with (createRow(lastRowNum + 1)) {
                createCell(0).apply {
                    setCellType(CellType.NUMERIC)
                    setCellValue(type.id.toDouble())
                }
                createCell(1).apply {
                    setCellType(CellType.STRING)
                    setCellValue(type.fullName)
                }
                createCell(2).apply {
                    setCellType(CellType.STRING)
                    setCellValue(type.shortName)
                }
            }
        }
    }

    override fun modifyGeographicalObject(obj: GeographicalObject) {
        forSheet(SHEET_GO) {
            val row = findRow { it.getCell(0).numericCellValue.toInt() == obj.id }
                    ?: throw IllegalStateException("Row with id ${obj.id} is not found!")
            with(row) {
                getCell(0).setCellValue(obj.id.toDouble())
                getCell(1).setCellValue(obj.typeId.toDouble())
                getCell(2).setCellValue(obj.name)
                obj.parent?.id?.let {
                    val cell = getCell(3)
                            ?: createCell(3).apply { setCellType(CellType.NUMERIC) }
                    cell.setCellValue(it.toDouble())
                }
            }
        }
    }

    override fun modifyGeographicalObjectType(type: GeographicalObjectType) {
        forSheet(SHEET_GO_TYPES) {
            val row = findRow { it.getCell(0).numericCellValue.toInt() == type.id }
                    ?: throw IllegalStateException("Row with id ${type.id} is not found!")
            with(row) {
                getCell(0).setCellValue(type.id.toDouble())
                getCell(1).setCellValue(type.fullName)
                getCell(2).setCellValue(type.shortName)
            }
        }
    }

    override fun deleteGeographicalObject(obj: GeographicalObject) =
            deleteRow(SHEET_GO, obj.id)

    override fun deleteGeographicalObjectType(type: GeographicalObjectType) =
            deleteRow(SHEET_GO_TYPES, type.id)

    override fun close() {
        isClosed = true

        // Apache POI tries to write something to the disk during closing. This can be the cause of an exception.
        try {
            workbook.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }


    private inline fun <T> forSheet(name: String, action: XSSFSheet.() -> T): T {
        val sheet = workbook.getSheet(name) ?: throw IllegalStateException("Sheet \"$name\" not found")
        return sheet.action()
    }

    private fun forEachRowInSheet(name: String, action: Row.() -> Unit) {
        forSheet(name) {
            rowIterator().asSequence()
                    .drop(1)
                    .forEach {
                        try { it.action() }
                        catch (e: IllegalStateException) {
                            println("Sheet: $name, row: ${it.rowNum}")
                            e.printStackTrace()
                        }
                    }
        }
    }

    private fun XSSFWorkbook.createTitledSheet(name: String, vararg titles: String): XSSFWorkbook {
        createSheet(name).createRow(0).apply {
            titles.forEachIndexed { i, s ->
                createCell(i, CellType.STRING).setCellValue(s)
            }
        }
        return this
    }

    private inline fun XSSFSheet.findRow(predicate: (Row) -> Boolean): Row? =
            rowIterator().asSequence()
                    .drop(1)
                    .find(predicate)

    private inline fun <T> T?.ifNotNull(action: (T) -> Unit): Boolean {
        return if (this != null) {
            action(this)
            true
        } else {
            false
        }
    }

    private fun deleteRow(sheetName: String, rowId: Int): Boolean {
        return forSheet(sheetName) {
            findRow { it.getCell(0).numericCellValue.toInt() == rowId }
                    .ifNotNull { removeRow(it) }
        }
    }

}
