package ru.narfu.kozalo.addresses.data.sources

import ru.narfu.kozalo.addresses.data.GeographicalObject
import ru.narfu.kozalo.addresses.data.GeographicalObjectType


interface DataSourceLoader<out T, out K> {
    fun prepareLoading() {}
    fun load(): DataSource<T, K>?
}


interface DataSourceSaver {
    fun save()
}


interface DataSource<out T, out K> {
    val loader: DataSourceLoader<T, K>
    val saver: DataSourceSaver

    val isClosed: Boolean

    fun getGeographicalObjects(useCache: Boolean = false): List<T>
    fun getGeographicalObjectTypes(useCache: Boolean = false): List<K>

    fun addGeographicalObject(obj: GeographicalObject)
    fun addGeographicalObjectType(type: GeographicalObjectType)

    fun modifyGeographicalObject(obj: GeographicalObject)
    fun modifyGeographicalObjectType(type: GeographicalObjectType)

    fun deleteGeographicalObject(obj: GeographicalObject): Boolean
    fun deleteGeographicalObjectType(type: GeographicalObjectType): Boolean

    fun close() {}
}
