package ru.narfu.kozalo.addresses.data


interface DatabaseObject {
    val id: Int
}


data class GeographicalObject(override val id: Int, var type: GeographicalObjectType, var name: String) : DatabaseObject {

    var parent: GeographicalObject? = null

    val typeId: Int
        get() = type.id

    val fullName: String
        get() = "${type.shortName} $name".trimStart()

    val fullAddress: String
        get() = fullAddressGetterImpl(this)


    private tailrec fun fullAddressGetterImpl(obj: GeographicalObject, str: String = ""): String {
        val parent = obj.parent
        val appendix = if (str.isEmpty()) obj.fullName else "${obj.fullName}, $str"
        return when (parent) {
            null -> appendix
            else -> fullAddressGetterImpl(parent, appendix)
        }
    }

}


data class GeographicalObjectType(override val id: Int, var fullName: String, var shortName: String = "") : DatabaseObject
