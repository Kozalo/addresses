package ru.narfu.kozalo.addresses.data.sources

import com.mysql.cj.core.MysqlType
import ru.narfu.kozalo.addresses.data.GeographicalObject
import ru.narfu.kozalo.addresses.data.GeographicalObjectType
import ru.narfu.kozalo.addresses.showError
import java.sql.*


private const val TABLE_GEOGRAPHICAL_OBJECTS = "GeographicalObjects"
private const val VIEW_GEOGRAPHICAL_OBJECTS = "GeographicalObjectsView"
private const val TABLE_TYPES = "Types"


class MySqlLoader(private val database: String,
                  private val login: String,
                  private val password: String?)
    : DataSourceLoader<GeographicalObject, GeographicalObjectType> {

    override fun load(): DataSource<GeographicalObject, GeographicalObjectType>? {
        val connectionUrl = "jdbc:mysql://localhost:3306/$database?serverTimezone=UTC&useUnicode=true&characterEncoding=utf-8"
        val connection = try {
            DriverManager.getConnection(connectionUrl, login, password)
        } catch (e: SQLException) {
            e.printStackTrace()
            showError("Не удалось соединиться с БД!", e)
            null
        }
        return connection?.let {
            it.autoCommit = false
            MySqlDataSource(it, this)
        }
    }

}


class MySqlSaver(private val connection: Connection) : DataSourceSaver {

    override fun save() {
        connection.commit()
    }

}


class MySqlDataSource(private val connection: Connection,
                      override val loader: DataSourceLoader<GeographicalObject, GeographicalObjectType>)
    : DataSource<GeographicalObject, GeographicalObjectType> {

    override val saver = MySqlSaver(connection)

    override var isClosed = false
        private set

    // The parameter is ignored in this implementation.
    override fun getGeographicalObjects(useCache: Boolean): List<GeographicalObject> {
        val query = "SELECT * FROM $VIEW_GEOGRAPHICAL_OBJECTS"
        val parents = mutableMapOf<Int, Int>()

        val geographicalObjects = connection.select(query) {
            try {
                val id = getInt("goId")
                val name = getString("goName")
                val typeId = getInt("typeId")
                val typeShortName = getString("typeShortName")
                val typeFullName = getString("typeFullName")
                val parent = getInt("goParent")

                if (parent > 0) {
                    parents[id] = parent
                }

                val type = GeographicalObjectType(typeId, typeFullName, typeShortName ?: "")
                GeographicalObject(id, type, name)
            } catch (e: SQLException) {
                e.printStackTrace()
                showError("Нарушение сруктуры данных в БД!", e)
                null
            }
        }

        geographicalObjects.filter { it.id in parents }
                .forEach { obj ->
                    obj.parent = geographicalObjects.find { it.id == parents[obj.id] }
                }
        return geographicalObjects
    }

    override fun getGeographicalObjectTypes(useCache: Boolean): List<GeographicalObjectType> {
        val query = "SELECT * FROM $TABLE_TYPES"
        return connection.select(query) {
            try {
                val id = getInt(1)
                val fullName = getString(2)
                val shortName = getString(3)
                GeographicalObjectType(id, fullName, shortName ?: "")
            } catch (e: SQLException) {
                e.printStackTrace()
                showError("Нарушение сруктуры данных в БД!", e)
                null
            }
        }
    }

    override fun addGeographicalObject(obj: GeographicalObject) {
        val query = "INSERT INTO $TABLE_GEOGRAPHICAL_OBJECTS (id, type, name, parent) VALUES (${obj.id}, ${obj.typeId}, ?, ?)"
        modifyGeographicalObjectImpl(obj, query)
    }

    override fun addGeographicalObjectType(type: GeographicalObjectType) {
        val query = "INSERT INTO $TABLE_TYPES (id, full_name, short_name) VALUES (${type.id}, ?, ?)"
        modifyGeographicalObjectTypeImpl(type, query)
    }

    override fun modifyGeographicalObject(obj: GeographicalObject) {
        val query = "UPDATE $TABLE_GEOGRAPHICAL_OBJECTS SET id = ${obj.id}, type = ${obj.typeId}, name = ?, parent = ? WHERE id = ${obj.id}"
        modifyGeographicalObjectImpl(obj, query)
    }

    override fun modifyGeographicalObjectType(type: GeographicalObjectType) {
        val query = "UPDATE $TABLE_TYPES SET id = ${type.id}, full_name = ?, short_name = ? WHERE id = ${type.id}"
        modifyGeographicalObjectTypeImpl(type, query)
    }

    override fun deleteGeographicalObject(obj: GeographicalObject) =
            deleteFrom(TABLE_GEOGRAPHICAL_OBJECTS, obj.id)

    override fun deleteGeographicalObjectType(type: GeographicalObjectType) =
            deleteFrom(TABLE_TYPES, type.id)

    override fun close() {
        isClosed = true
        with (connection) {
            rollback()
            close()
        }
    }


    private fun modifyGeographicalObjectImpl(obj: GeographicalObject, query: String) {
        connection.update(query) {
            setString(1, obj.name)
            val parent = obj.parent
            if (parent != null) {
                setInt(2, parent.id)
            } else {
                setNull(2, MysqlType.INT_UNSIGNED.jdbcType)
            }
        }
    }

    private fun modifyGeographicalObjectTypeImpl(type: GeographicalObjectType, query: String) {
        connection.update(query) {
            setString(1, type.fullName)
            if (type.shortName.isNotBlank()) {
                setString(2, type.shortName)
            } else {
                setNull(2, MysqlType.VARCHAR.jdbcType)
            }
        }
    }

    private fun deleteFrom(table: String, id: Int): Boolean {
        val query = "DELETE FROM $table WHERE id = $id"
        val affectedRowsCount = connection.createStatement().executeUpdate(query)
        return affectedRowsCount > 0
    }

}


private inline fun <T> ResultSet.map(action: ResultSet.() -> T?): List<T> {
    val collection = mutableListOf<T>()
    while (next()) {
        val t = action(this)
        if (t != null) {
            collection.add(t)
        }
    }
    return collection
}

private inline fun <T> Connection.select(query: String, mapping: ResultSet.() -> T?) =
        createStatement()
                .executeQuery(query)
                .map(mapping)

private inline fun Connection.update(query: String, params: PreparedStatement.() -> Unit) =
        prepareStatement(query)
                .apply(params)
                .executeUpdate()
