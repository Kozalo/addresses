package ru.narfu.kozalo.addresses.data.manipulators

import javafx.beans.property.SimpleObjectProperty
import ru.narfu.kozalo.addresses.AppController
import ru.narfu.kozalo.addresses.data.DatabaseObject
import ru.narfu.kozalo.addresses.data.GeographicalObject
import ru.narfu.kozalo.addresses.data.GeographicalObjectType
import ru.narfu.kozalo.addresses.showWarning
import ru.narfu.kozalo.addresses.views.NewGeographicalObjectTypeView
import ru.narfu.kozalo.addresses.views.NewGeographicalObjectView


class DataModifier(private val controller: AppController) {

    fun addGeographicalObject() {
        val geographicalObjects = controller.geographicalObjects
        val newId = getNewId(geographicalObjects)
        val createView = NewGeographicalObjectView(newId, controller.geographicalObjectTypes, geographicalObjects)
        createView.openModal()
    }

    fun deleteGeographicalObject() {
        val selectedItem = controller.selectedGeographicalObject.value
        val children = controller.geographicalObjects.filter { it.parent == selectedItem }
        if (children.isNotEmpty()) {
            val childrenListStr = children.joinToString(", ") { it.name }
            showWarning("Нельзя удалить объект, являющийся родительским для следующих объектов: $childrenListStr!")
            return
        }

        modifySelectedObject {
            controller.geographicalObjects.remove(it)
            controller.dataSource.deleteGeographicalObject(it)
        }
    }

    fun addGeographicalObjectType() {
        val newId = getNewId(controller.geographicalObjectTypes)
        val newTypeView = NewGeographicalObjectTypeView(newId)
        newTypeView.openModal()
    }

    fun deleteGeographicalObjectType() {
        val selectedItem = controller.selectedGeographicalObjectType.value
        val objectsWithThisType = controller.geographicalObjects.filter { it.type == selectedItem }
        if (objectsWithThisType.isNotEmpty()) {
            val objectsListStr = objectsWithThisType.joinToString(", ") { it.name }
            showWarning("Нельзя удалить тип, использующийся следующими объектами: $objectsListStr!")
            return
        }

        modifySelectedType {
            controller.geographicalObjectTypes.remove(it)
            controller.dataSource.deleteGeographicalObjectType(it)
        }
    }

    fun editGeographicalObjectType(instance: GeographicalObjectType? = null) = modifySelectedType(instance) {
        controller.dataSource.modifyGeographicalObjectType(it)
    }


    // It's used only once but still present for symmetry.
    private inline fun modifySelectedObject(instance: GeographicalObject? = null,
                                            action: (GeographicalObject) -> Unit) =
            modify(controller.selectedGeographicalObject, "Географический объект не выбран!", instance, action)

    private inline fun modifySelectedType(instance: GeographicalObjectType? = null,
                                          action: (GeographicalObjectType) -> Unit) =
            modify(controller.selectedGeographicalObjectType, "Тип не выбран!", instance, action)

    private inline fun <T> modify(
            objectProperty: SimpleObjectProperty<T>,
            errorMessage: String,
            instance: T?,
            action: (T) -> Unit) {

        if (instance != null) {
            action(instance)
            return
        }
        val selectedItem = objectProperty.value
        if (selectedItem != null) {
            action(selectedItem)
        } else {
            showWarning(errorMessage)
        }

    }


    private fun <T : DatabaseObject> getNewId(list: List<T>): Int {
        return if (list.isNotEmpty()) {
            list.map { it.id }
                .sorted()
                .last()
                .inc()
        } else {
            1
        }
    }

}
