package ru.narfu.kozalo.addresses

import javafx.beans.property.SimpleBooleanProperty
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.event.EventHandler
import javafx.scene.control.TreeItem
import javafx.scene.control.TreeView
import javafx.stage.StageStyle
import ru.narfu.kozalo.addresses.data.manipulators.DataValidator
import ru.narfu.kozalo.addresses.data.GeographicalObject
import ru.narfu.kozalo.addresses.data.GeographicalObjectType
import ru.narfu.kozalo.addresses.data.manipulators.DataModifier
import ru.narfu.kozalo.addresses.data.sources.DataSource
import ru.narfu.kozalo.addresses.data.sources.DataSourceLoader
import ru.narfu.kozalo.addresses.data.sources.ExcelLoader
import ru.narfu.kozalo.addresses.data.sources.MySqlLoader
import ru.narfu.kozalo.addresses.server.GeographicalObjectsServer
import ru.narfu.kozalo.addresses.server.WebsiteServer
import ru.narfu.kozalo.addresses.views.DatabaseCredentialsView
import ru.narfu.kozalo.addresses.views.EditGeographicalObjectView
import tornadofx.*
import java.io.IOException
import java.net.URI


class AppController : Controller() {

    val geographicalObjects = mutableListOf<GeographicalObject>().observable()
    val geographicalObjectTypes = mutableListOf<GeographicalObjectType>().observable()

    internal var treeView: TreeView<GeographicalObject> by singleAssign()

    internal val selectedGeographicalObject = SimpleObjectProperty<GeographicalObject>()
    internal val selectedGeographicalObjectType = SimpleObjectProperty<GeographicalObjectType>()
    internal val dataSourceLoaded = SimpleBooleanProperty(false)

    private var _dataSource: DataSource<GeographicalObject, GeographicalObjectType>? = null
    internal val dataSource: DataSource<GeographicalObject, GeographicalObjectType>
        get() = _dataSource ?: throw IllegalStateException("DataSource has not been loaded yet!")

    private var websiteServer: WebsiteServer? = null
    internal val port = SimpleStringProperty("Сервер выключен")

    private val dataModifier = DataModifier(this)

    init {
        val refreshTree: () -> Unit = {
            treeView.apply {
                root = TreeItem(geographicalObjects.find { it.parent == null })
                populate { item ->
                    item.isExpanded = true
                    geographicalObjects.filter { it.parent == item.value }
                }
            }
        }
        geographicalObjects.onChange { refreshTree() }
        subscribe<InvalidateGeographicalObjectsTable> { refreshTree() }

        subscribe<GeographicalObjectCreated> {
            geographicalObjects.add(it.obj)
            dataSource.addGeographicalObject(it.obj)
            fire(InvalidateGeographicalObjectsTable)
        }
        subscribe<GeographicalObjectChanged> {
            dataSource.modifyGeographicalObject(it.obj)
        }

        subscribe<GeographicalObjectTypeCreated> {
            geographicalObjectTypes.add(it.type)
            dataSource.addGeographicalObjectType(it.type)
        }
        subscribe<GeographicalObjectTypeChanged> {
            dataSource.modifyGeographicalObjectType(it.type)
        }

        subscribe<OpenWebsite> {
            val uri = URI("http://localhost:${websiteServer!!.port}")
            hostServices.showDocument(uri.toString())
        }

        primaryStage.onCloseRequest = EventHandler {
            closeDataSource(reflectOnUI = false)
        }
    }

    fun openExcelFile() = loadDataSource(ExcelLoader())

    fun openMySqlDatabase() {
        val credentialsView = DatabaseCredentialsView()
        val loginToDatabaseCallback: EventContext.(LoginToDatabase) -> Unit = {
            val mySqlLoader = MySqlLoader(it.databaseName, it.login, it.password)
            loadDataSource(mySqlLoader)
        }
        subscribe(action = loginToDatabaseCallback)
        credentialsView.onCloseCallback = {
            unsubscribe(action = loginToDatabaseCallback)
        }
        credentialsView.openModal(StageStyle.UNDECORATED)
    }

    fun addGeographicalObject() = dataModifier.addGeographicalObject()

    fun deleteGeographicalObject() = dataModifier.deleteGeographicalObject()

    fun addGeographicalObjectType() = dataModifier.addGeographicalObjectType()

    fun deleteGeographicalObjectType() = dataModifier.deleteGeographicalObjectType()

    fun editGeographicalObjectType(instance: GeographicalObjectType? = null) =
            dataModifier.editGeographicalObjectType(instance)

    fun save() {
        val validator = DataValidator(geographicalObjects)
        if (!validator.isDataValid()) {
            showWarning(validator.issue)
            return
        }

        fire(PauseView)
        runAsync {
            try {
                dataSource.saver.save()
            } catch (e: IOException) {
                showError("Нет доступа на запись к файлу!", e)
            }
        } ui {
            if (dataSource.isClosed) {
                loadDataSource(dataSource.loader, skipPreparation = true)
            }
            fire(ResumeView)
        }
    }

    fun openEditGeographicalObjectView() {
        val selectedItem = selectedGeographicalObject.value
        if (selectedItem != null) {
            val parents = geographicalObjects.filter { it != selectedItem }
            val editView = EditGeographicalObjectView(geographicalObjectTypes, parents)
            editView.openModal()
        }
    }

    fun toggleServer(enable: Boolean) {
        if (enable) {
            val server = GeographicalObjectsServer(geographicalObjects, geographicalObjectTypes)
            port.value = "Порт: ${server.port}"
            websiteServer = server
        } else {
            websiteServer?.stop()
            port.value = "Сервер выключен"
        }
    }


    private fun loadDataSource(loader: DataSourceLoader<GeographicalObject, GeographicalObjectType>,
                               skipPreparation: Boolean = false) {
        fire(PauseView)
        if (_dataSource != null) {
            closeDataSource()
        }
        if (!skipPreparation) {
            loader.prepareLoading()
        }
        runAsync {
            _dataSource = loader.load()
        } ui continuation@{
            fire(ResumeView)
            if (_dataSource == null) {
                return@continuation
            }

            dataSourceLoaded.value = true
            fire(DataSourceLoaded)

            geographicalObjects.clear()
            geographicalObjects.addAll(dataSource.getGeographicalObjects())
            geographicalObjectTypes.clear()
            geographicalObjectTypes.addAll(dataSource.getGeographicalObjectTypes(true))
        }
    }

    private fun closeDataSource(reflectOnUI: Boolean = true) {
        _dataSource?.close()
        websiteServer?.stop()

        if (reflectOnUI) {
            geographicalObjects.clear()
            geographicalObjectTypes.clear()
        }
    }

}
