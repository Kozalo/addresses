SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `geographical_objects2`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_geographical_object`(IN `name` VARCHAR(100) CHARSET utf8mb4, IN `type_name` VARCHAR(50) CHARSET utf8mb4, IN `short_type` VARCHAR(10) CHARSET utf8mb4)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
BEGIN
    DECLARE type_id INT;
    SELECT `id` INTO @type_id FROM `Types` WHERE `full_name` = @type_name AND `short_name` = @type_short;

    IF @type_id IS NOT NULL THEN
        INSERT INTO `Geographical_objects` (`type`, `name`) VALUES (@type_id, @name);
    ELSE
        INSERT INTO `Types` (`full_name`, `short_name`) VALUES (@type_name, @type_short);
        INSERT INTO `Geographical_objects` (`type`, `name`) VALUES (LAST_INSERT_ID(), @name);
    END IF;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `GeographicalObjects`
--

CREATE TABLE IF NOT EXISTS `GeographicalObjects` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `type` int(10) unsigned NOT NULL,
  `parent` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `GeographicalObjects`
--

INSERT INTO `GeographicalObjects` (`id`, `name`, `type`, `parent`) VALUES
(1, 'Россия', 1, NULL),
(2, 'Архангельская', 2, 1),
(3, 'Архангельск', 3, 2),
(4, 'Северодвинск', 3, 2),
(5, 'Советская', 4, 4),
(6, 'Железнодорожная', 4, 4),
(7, 'Ломоносова', 8, 3),
(8, '15', 6, 5),
(9, '33', 6, 7),
(10, 'Торцева', 4, 4),
(11, 'Труда', 8, 4),
(12, '8', 6, 10),
(13, '19', 6, 11),
(14, '69', 7, 9);

--
-- Triggers `GeographicalObjects`
--
DELIMITER //
CREATE TRIGGER `recursive_object_safeguard` BEFORE UPDATE ON `GeographicalObjects`
 FOR EACH ROW BEGIN
    DECLARE msg varchar(255);
    IF new.parent = new.id THEN
        SET msg = 'parent_id and object id must be different!';
        SIGNAL SQLSTATE '45000' SET message_text = msg;
    END IF;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `Types`
--

CREATE TABLE IF NOT EXISTS `Types` (
  `id` int(10) unsigned NOT NULL,
  `full_name` varchar(50) NOT NULL,
  `short_name` varchar(10) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Types`
--

INSERT INTO `Types` (`id`, `full_name`, `short_name`) VALUES
(1, 'Страна', NULL),
(2, 'Область', 'обл.'),
(3, 'Город', 'г.'),
(4, 'Улица', 'ул.'),
(5, 'Бульвар', 'б.'),
(6, 'Дом', 'д.'),
(7, 'Квартира', 'кв.'),
(8, 'Проспект', 'пр.');

-- --------------------------------------------------------

--
-- Structure for view `GeographicalObjectsView`
--
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `GeographicalObjectsView` AS select `go`.`id` AS `goId`,`go`.`name` AS `goName`,`go`.`parent` AS `goParent`,`t`.`id` AS `typeId`,`t`.`full_name` AS `typeFullName`,`t`.`short_name` AS `typeShortName` from (`GeographicalObjects` `go` join `Types` `t` on((`go`.`type` = `t`.`id`)));

--
-- Indexes for dumped tables
--

--
-- Indexes for table `GeographicalObjects`
--
ALTER TABLE `GeographicalObjects`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `GeographicalObjects_name_uindex` (`name`), ADD KEY `GeographicalObjects_Types_id_fk` (`type`), ADD KEY `GeographicalObjects_GeographicalObjects_id_fk` (`parent`);

--
-- Indexes for table `Types`
--
ALTER TABLE `Types`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `Types_full_name_uindex` (`full_name`), ADD UNIQUE KEY `Types_short_name_uindex` (`short_name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `GeographicalObjects`
--
ALTER TABLE `GeographicalObjects`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `Types`
--
ALTER TABLE `Types`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `GeographicalObjects`
--
ALTER TABLE `GeographicalObjects`
ADD CONSTRAINT `GeographicalObjects_GeographicalObjects_id_fk` FOREIGN KEY (`parent`) REFERENCES `GeographicalObjects` (`id`),
ADD CONSTRAINT `GeographicalObjects_Types_id_fk` FOREIGN KEY (`type`) REFERENCES `Types` (`id`);
